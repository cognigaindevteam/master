-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 04, 2017 at 05:26 PM
-- Server version: 10.1.28-MariaDB
-- PHP Version: 7.1.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `congnigain_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `ci_sessions`
--

CREATE TABLE `ci_sessions` (
  `session_id` varchar(40) NOT NULL DEFAULT '0',
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `user_agent` varchar(120) NOT NULL,
  `last_activity` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `user_data` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user`
--

CREATE TABLE `tbl_user` (
  `id` int(111) NOT NULL,
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `user_email` varchar(255) NOT NULL,
  `user_password` varchar(255) NOT NULL,
  `flag` tinyint(4) NOT NULL,
  `user_phone` varchar(50) NOT NULL,
  `user_notes` text NOT NULL,
  `activation_token` varchar(255) NOT NULL,
  `activation_time` varchar(255) NOT NULL,
  `keystring` varchar(255) NOT NULL,
  `isused` int(10) NOT NULL,
  `deactivate` int(11) NOT NULL DEFAULT '0',
  `updatedtimestamp` datetime NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_user`
--

INSERT INTO `tbl_user` (`id`, `first_name`, `last_name`, `user_email`, `user_password`, `flag`, `user_phone`, `user_notes`, `activation_token`, `activation_time`, `keystring`, `isused`, `deactivate`, `updatedtimestamp`, `created_date`, `updated_date`) VALUES
(1, 'jainendra', 'pal', 'jpal@evonix.co', 'e10adc3949ba59abbe56e057f20f883e', 1, '0', '', 'q009314eche8413l', '', 'gzpmp4jnena791jy', 0, 0, '2017-11-27 15:35:36', '2017-11-27 15:35:36', '0000-00-00 00:00:00'),
(16, 'arvin', 'salunke', 'jpal@evonix.co', 'e10adc3949ba59abbe56e057f20f883e', 0, '9455947282', 'hgfhfsd', 'gjoqtcec5gvls07s', '', '', 0, 0, '0000-00-00 00:00:00', '2017-11-29 01:57:30', '0000-00-00 00:00:00'),
(17, 'arvin', 'salunke', 'jpal@evonix.co', 'e10adc3949ba59abbe56e057f20f883e', 0, '9455947282', 'hgfhfsd', '7wm4n6xffc6bk8w0', '', '', 0, 0, '0000-00-00 00:00:00', '2017-11-29 01:57:30', '0000-00-00 00:00:00'),
(18, 'archit', 'admin', 'jpal@evonix.co', 'e10adc3949ba59abbe56e057f20f883e', 0, '9455947282', 'jssajfdsdfhdsjf', 's61isx1ddrevyou0', '', '', 0, 0, '0000-00-00 00:00:00', '2017-11-29 02:02:46', '0000-00-00 00:00:00'),
(19, 'archit', 'admin', 'jpal@evonix.co', 'e10adc3949ba59abbe56e057f20f883e', 0, '9455947282', 'jssajfdsdfhdsjf', 'mvyblr0u868pv246', '', '', 0, 0, '0000-00-00 00:00:00', '2017-11-29 02:02:46', '0000-00-00 00:00:00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `ci_sessions`
--
ALTER TABLE `ci_sessions`
  ADD PRIMARY KEY (`session_id`),
  ADD KEY `last_activity_idx` (`last_activity`);

--
-- Indexes for table `tbl_user`
--
ALTER TABLE `tbl_user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_user`
--
ALTER TABLE `tbl_user`
  MODIFY `id` int(111) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

<?php 
ob_start();

defined('BASEPATH') OR exit('No direct script access allowed');

/**
* 
*/
class User_model extends CI_Model
{
	
	function __construct()
  {
        // Call the Model constructor
    parent::__construct();

    $this->load->database();
  }

  // FUNCTION FOR SAVE SIGN UP USER INFO

  public function saveUserInfo($data)
  {
     
   


    $saveuserInfo = $this->db->insert('tbl_user',$data);
      return $saveuserInfo;
   

  	//echo "<pre>jaine"; print_r($info); exit;

    
   // print_r($this->db->last_query()); exit;

        


  }

public function update_password($emailaddress,$npassword) {
                                      $newpassword = array(
                                        'user_password' => md5($npassword),
                                        'isused' => 1
                                        );
                                      $this->db->where('user_email', $emailaddress);
                                      $this->db->update('tbl_user', $newpassword);
        //print_r($this->db->last_query()); exit;
                                      return TRUE;
                                    }

 /*=============== FUNCTION FOR MATCH KEY STRING ================*/


                                    public function check_randomkey_string($randomkeystring) 
                                    {

                                      $query = $this->db->query("SELECT * FROM tbl_user WHERE keystring='$randomkeystring'");
        //echo $query->num_rows(); exit;
                                      if ($query->num_rows() > 0) {
                                        return $query->row();
                                      } else {
                                        return false;
                                      }
                                    }


  /*========= FUNCTION FOR ACTIVATE LOGIN ========*/

public function activateLogin($data,$token)

{

 $status =  $this->db->where('activation_token',$token)
 ->update('tbl_user',$data);

 return $status;
}


/*========FUNCTION FOR LOGIN=============*/

  public function check_user_login($emailaddress, $password) {

    $matchpass = md5($password);

  //echo $emailaddress."  ".$password; exit;
    $query = $this->db->query("SELECT * FROM tbl_user WHERE   flag =1 and   user_email='$emailaddress' and 
      user_password='".$matchpass."'"); 
        //echo $query->num_rows(); exit;
    if ($query->num_rows() > 0) {
      return $query->row();
    } else {
      return false;
    }
  }


  // check exist mailid 

 /*============= FUNCTION FOR FORGOT PASSWORD =============*/

      public function check_admin_forgot_password($emailaddress) {

       $query = $this->db->query("SELECT * FROM tbl_user WHERE user_email='$emailaddress'");
        //echo $query->num_rows(); exit;
                                      if ($query->num_rows() > 0) {
                                        return $query->row();
                                      } else {
                                        return false;
                                      }
                                    }




               /*=================FUNCTION FOR UPDATE KEY STRING================*/


                                    public function update_keystring($emailaddress,$randomkeystring) {

                                              date_default_timezone_set('UTC');
          
                                      $newkeystring = array(
                                        'keystring' => $randomkeystring,
                                        'isused' => 0,
                                        'updatedtimestamp' => date("Y-m-d H:i:s")
                                        );
                                      $this->db->where('user_email', $emailaddress);
                                      $this->db->update('tbl_user', $newkeystring);
                                      return TRUE;
                                    }                      


/******************function for get all courses**************/

 public function getAllCourses()
 {

   
   $courses = $this->db->select('*')
                      ->from('tbl_course')
                      ->where('is_delete',0)
                      ->get()->result();
        // /  echo "<pre>"; print_r($courses); exit;
                      return $courses;

 }




 /*=========== FUNCTION FOR CHECK DUPLICATE EMAIL =============*/

                                    public function findEmail($email)
                                    {

                                      $checkEmail = $this->db->select('*')
                                                             ->from('tbl_user')
                                                             ->where('user_email',$email)
                                                             ->get()->row();
                                      return $checkEmail;

                                    }



  /*==================FUNCTION FOR SEARCH===================== */
  public function searchResult($courseName,$trainerName,$date)
  {
     // like('title', 'match'); $this->db->or_like('body', $match);
      error_reporting(0);
        $result = $this->db->select('*')
                         ->from('tbl_course')
                         ->like('course_name',$courseName)
                         ->or_like('trainer',$trainerName)
                         ->or_like('training_dates',$date)
                         ->get()->result();

                
               // print_r($this->db->last_query()); 
                     //  echo "<pre>"; print_r($result); exit();

                         return $result;
            
 
  }


/*===================== FUNCTION FOR GET USER PROFILE DATA ===================*/
 public function getUserProfileData($user_id)
 {

    $userProfileInfo = $this->db->select('*')
                                ->from('tbl_user')
                                ->where('id',$user_id)
                                ->get()->row();



                                return $userProfileInfo;



 }



 /*============== FUNCTION FOR UPDATE USER PROFILE =================*/
   
   public function updateUserProfile($user_id,$info)
   {

      $updateProfile = $this->db->where('id',$user_id)
                                ->update('tbl_user',$info);
                
                return $updateProfile;


   }        


/*=========FUNCTION FOR SAVE USER PROFILE PIC ==========*/

public function saveProfilePic($file_name,$uid)
{

 // echo $file_name."<br>".$uid; exit;;

 $data= $this->db->select('*')
                 ->from('tbl_user')
                 ->where('id',$uid)
                 ->get()->row();

 $arr = array('id'=>$uid,'user_profilePic' =>$file_name );

 if($data !=null )
 {

  $up=$this->db->where('id',$uid)
               ->update('tbl_user',$arr);

  return $up;
}else{

 $up = $this->db->insert('tbl_user',$arr);

 return $up; 

}

}  

/*function for save course*/

 public function saveUserCourse($courseInfo)
 {

     $saveUserCourse = $this->db->insert('tbl_user_course',$courseInfo);

          return $saveUserCourse;


 }

/* =============== FUNCTION FOR GET USER DASHBOARD DATA ================*/
public function getUserDashboardData($user_id)
{


   $userDashboardData = $this->db->select('tbl_course.*,tbl_user_course.*')
                                        ->from('tbl_user_course')
                                        ->join('tbl_course','tbl_course.id=tbl_user_course.course_id')
                                        ->where('tbl_user_course.user_id',$user_id)
                                        ->where('tbl_course.is_delete',0)
                                        ->get()->result();
                                        return $userDashboardData;


}


/*============= GET USER LEARNING HISTORY ===================*/
  
  public function getUserLearningHistory($user_id)
  {
    

     $userLearningHistory = $this->db->select('tbl_course.*,tbl_user_course.*')
                                        ->from('tbl_user_course')
                                        ->join('tbl_course','tbl_course.id=tbl_user_course.course_id')
                                        ->where('tbl_user_course.user_id',$user_id)
                                        ->where('tbl_user_course.course_status','c')
                                        ->get()->result();

                                return $userLearningHistory;
  }



 /* function for get user enrolled courses*/


  public function getUserEnrolledCourses($user_id)
  {
       
       $userEnrolledCourses = $this->db->select('tbl_course.*,tbl_user_course.*, tbl_course.id as c_id')
                                        ->from('tbl_user_course')
                                        ->join('tbl_course','tbl_course.id=tbl_user_course.course_id')
                                        ->where('tbl_user_course.user_id',$user_id)
                                        ->where('tbl_user_course.course_status','a')
                                        //->where_or('tbl_user_course.course_status','b')
                                        ->get()->result();
                             
                           // echo "<pre>"; print_r($userEnrolledCourses); exit;
                                        return $userEnrolledCourses;


  }


   
 /* function for get user enrolled courses*/


  public function getUserEnrolledCoursesForAssesment($user_id)
  {
       
    /*   $coursess = $this->db->select('*')
                            ->from('mst_subject')
                            ->get()->result();

                            return $coursess;
*/
     
     $cName = $this->db->select('tbl_course.*,tbl_user_course.*')
                        ->from('tbl_user_course')
                        ->join('tbl_course','tbl_user_course.course_id=tbl_course.id')
                        ->where('tbl_user_course.user_id',$user_id)
                        
                        ->get()->result();
                        
                      //  echo "<pre>"; print_r($cName); exit;
                        return $cName;


  }

   /* function for get subject assesments  */


   public function getSubjectAssesments($sid)
   {

    $assesments = $this->db->select('*')
                            ->from('mst_test')
                            ->where('sub_id',$sid)
                            ->where('is_delete',0)
                            ->get()->result();


                           return $assesments;





   }



  /*============== function for check user enrolled coursess ================*/
     
     public function checkEnrolledCourse($user_id)
     {

        $userEnrolledCourses = $this->db->select('tbl_user_course.*,tbl_course.id as pid')
                                        ->from('tbl_user_course')
                                        ->join('tbl_course','tbl_user_course.course_id=tbl_course.id')
                                        ->where('user_id',$user_id)

                                        ->get()->result();
                               $data = array();
                 if(!empty($userEnrolledCourses))
                 {
                                         

                 foreach ($userEnrolledCourses as $key => $value) {
               
                            $data[$value->course_id] = $value->pid;
                           }
                                
                             
                                            return $data;
                    } 
                   }



 /*============ FUNCTION CHANGE COURSE STATUS ==============*/

 public function changeCourseSatus($data,$user_id,$cId)
 {

     $changeStatus = $this->db->where('user_id',$user_id)
                               ->where('id',$cId)
                               ->update('tbl_user_course',$data);


                               return $changeStatus;


 }



 /*============== FUNCTION FOR GET COURSE DATA ============*/

  public function getCourseData($id)
  {

  $courseData = $this->db->select('*')
                         ->from('tbl_course')
                         ->where('id',$id)
                         ->get()->row();




  }

 /*============ GET COURSE NAME ==========*/

  public function getCourseName($cId)
  {
    
    $courseName = $this->db->select('*')
                            ->from('tbl_course')
                            ->where('id',$cId)
                            ->where('is_delete',0)
                            ->get()->row();

                            return $courseName; 

  }

  /*===========get Courses topics ============*/
    public function getCourseTopics($user_id,$cId)
    {
       $coursesTopics = $this->db->select('tbl_topic.*')
                                  ->from('tbl_course')
                                  ->join('tbl_topic','tbl_course.id=tbl_topic.course_id')
                                  ->where('tbl_course.id',$cId)
                                  ->get()->result();


                                //  print_r($this->db->last_query()); exit();

                                  return  $coursesTopics;


    }


    /*=======================  FUNCTION FOR GET TOPICS DESCRIPTION ===============*/

    public function getTopicsDescriptions($cId)
    {
     
      //echo $cId; exit;

      $topics = $this->db->select('tbl_topic.*,tbl_course.id as cId')
                         ->from('tbl_topic')
                         ->join('tbl_course','tbl_topic.course_id=tbl_course.course_id')
                         ->where('tbl_course.id',$cId)
                         ->get()->result();

                        // / print_r($this->db->last_query()); exit;
                   return $topics; 
                       //  echo "<pre>"; print_r($topics); exit;

    }



    /*================== FUNCTION FOR GET COURSE ASSESMENTS =====================*/

    public function getCourseAssesments($cId)
    {

       $assesments = $this->db->select('*')
                              ->from('mst_test')
                              ->where('sub_id',$cId)
                              ->get()->result();


                           //  echo "<pre>"; print_r($assesments); exit;

                              return $assesments;



    }


    public function getQuestions($tid)
    {
     
      $questions = $this->db->select('*')
                           ->from('mst_question')
                           ->where('test_id',$tid)
                           ->get()->row();
   
                           // echo "<pre>"; print_r($questions); exit;

                           return $questions;
                  

                 // $rs=mysql_query("select * from mst_question where test_id=$tid")


    }

  /*function for get questions from mst*/

  public function getQuestionFroMst($tid)
  {
     

   $rs = $this->db->select('*')
                  ->from('mst_question')
                  ->where('test_id',$tid)
                  ->get()->row();

                  return $rs;
  }

  /*function for get correct answer*/

  public function getCorrectAns($ques_id)
   
   {

     $cAns = $this->db->select('*')
                     ->from('mst_question')
                     ->where('que_id',$ques_id)
                     ->get()->row();

                     return $cAns;
   }


  /*function for save user ans*/

  public function saveUserAns($ansArray)
  {
     date_default_timezone_set("Asia/Kolkata");
  
   $saveAns = $this->db->insert('tbl_user_answers',$ansArray);

   return $saveAns;

    

  }

  /* public function for set assesments */
  public function saveAssesmentsSatus($saveData)
  {

    $saveStatus= $this->db->insert('tbl_user_assesments',$saveData);

        return   $saveStatus;

  }

  /* function for get assesments*/

  public function getAssesmentsStatus($user_id)
    {
      
      $status = $this->db->select('*')
                         ->from('tbl_user_assesments')
                         ->where('user_id',$user_id)
                         ->get()->result();

                         return $status;
                        // echo "<pre>"; print_r($status); exit;
                         $data=array();

                      if(!empty($status))
                     {
                                         

                 foreach ($status as $key => $value) {
               
                            $data[$value->course_id] = $value->pid;
                           }
                                
                             
                                      //      return $data;
                    }



  }

  /* fuction for get course name*/
  public function getCourseNameforAssesment($sid,$user_id)
  {
     
      $cName = $this->db->select('tbl_course.course_name')
                        ->from('tbl_user_course')
                        ->join('tbl_course','tbl_user_course.course_id=tbl_course.id')
                        ->where('tbl_user_course.user_id',$user_id)
                        ->where('tbl_user_course.course_id',$sid)
                        ->get()->row();

                        return $cName;

  }


  // function for view result 

  public function getStudentResult($user_id,$course_id)
  {
     
     /*$q = "SELECT tua.*,mq.true_ans,tc.course_name,(SELECT COUNT(mst_question.course_id) FROM mst_question WHERE mst_question.course_id=tua.course_id) as totalQes FROM tbl_user_answers tua 
JOIN mst_question mq on tua.ques_id=mq.que_id
JOIN  tbl_course tc on tua.course_id=tc.id
WHERE tua.user_id='".$user_id."' and tua.course_id='".$course_id."'";*/

  $r=$this->db->query("SELECT tua.*,mq.true_ans,tc.course_name,(SELECT COUNT(mst_question.course_id) FROM mst_question WHERE mst_question.course_id=tua.course_id) as totalQes FROM tbl_user_answers tua 
JOIN mst_question mq on tua.ques_id=mq.que_id
JOIN  tbl_course tc on tua.course_id=tc.id
WHERE tua.user_id='".$user_id."' and tua.course_id='".$course_id."'"); 
  $result = $r->result();
    return $result;
  //echo "<pre>"; print_r($result); exit();
    

  }

  // public function for get total questions
 
 public function getTotalQuestions($subject_id)
 {
   
   $questions = $this->db->select('count(course_id) as noOfQues')
                         ->from('mst_question')
                         ->where('course_id',$subject_id)
                         ->get()->row();
                         return $questions;
            // echo $questions; exit();
 }

// functin for get answerd questions 

 public function getUserAnwerdsQues($subject_id,$user_id)
 {


     $totalAnswerdQues = $this->db->select('count(course_id) as ansQues')
                                  ->from('tbl_user_answers')
                                  ->where('course_id',$subject_id)
                                  ->where('user_id',$user_id)
                                  ->get()->row();

                                  return $totalAnswerdQues; 



 }

  // function for save user assesments attempt
  
  public function saveUserAtteptAssem($attemptData)
  {
     $saveRcd=$this->db->insert('tbl_attempt',$attemptData);
        
        return $saveRcd;
  }


  // function for get saved user answers 

  public function getSavedUserAns($subject_id,$user_id)
  {

     $result = $this->db->select('*')
                        ->from('tbl_user_answers')
                        ->limit(1)
                        ->order_by('id','DESC')
                        ->get()->row();

                       // echo "<pre>"; print_r($result); exit;

                        return $result;
  }


  public function checkUserAns($question_id,$user_id,$subject_id)
  {

    $rcds = $this->db->select('*')
                    ->from('tbl_user_answers')
                    ->where('ques_id',$question_id)
                    ->where('user_id',$user_id)
                    ->where('course_id',$subject_id)
                    ->get()->row();

                    return $rcds; 


  }
   // function for delete old records
  public function deleteOldAns($question_id,$user_id,$subject_id)
  {
        
        $delrcd = $this->db->where('ques_id',$question_id)
                           ->where('user_id',$user_id)
                           ->where('course_id',$subject_id)
                           ->delete('tbl_user_answers');

          // /print_r($this->db->last_query()); exit();
                           return $delrcd;

  }
}

?>
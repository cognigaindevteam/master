-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Feb 05, 2018 at 03:01 PM
-- Server version: 10.1.28-MariaDB
-- PHP Version: 5.6.32

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `congnigain_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `mst_admin`
--

CREATE TABLE `mst_admin` (
  `id` int(11) NOT NULL,
  `loginid` varchar(50) NOT NULL,
  `pass` varchar(50) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mst_admin`
--

INSERT INTO `mst_admin` (`id`, `loginid`, `pass`) VALUES
(1, 'sanjeev', 'sanjeev');

-- --------------------------------------------------------

--
-- Table structure for table `mst_question`
--

CREATE TABLE `mst_question` (
  `que_id` int(5) NOT NULL,
  `course_id` int(11) NOT NULL,
  `test_id` int(5) DEFAULT NULL,
  `que_desc` varchar(150) DEFAULT NULL,
  `ans1` varchar(75) DEFAULT NULL,
  `ans2` varchar(75) DEFAULT NULL,
  `ans3` varchar(75) DEFAULT NULL,
  `ans4` varchar(75) DEFAULT NULL,
  `true_ans` int(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mst_question`
--

INSERT INTO `mst_question` (`que_id`, `course_id`, `test_id`, `que_desc`, `ans1`, `ans2`, `ans3`, `ans4`, `true_ans`) VALUES
(16, 1, 8, 'What  Default Data Type ?', 'String', 'Variant', 'Integer', 'Boolear', 2),
(17, 1, 8, 'What is Default Form Border Style ?', 'Fixed Single', 'None', 'Sizeable', 'Fixed Diaglog', 3),
(18, 1, 8, 'Which is not type of Control ?', 'text', 'lable', 'checkbox', 'option button', 1),
(19, 2, 9, 'Which of the follwing contexts are available in the add watch window?', 'Project', 'Module', 'Procedure', 'All', 4),
(20, 2, 9, 'Which window will allow you to halt the execution of your code when a variable changes?', 'The call stack window', 'The immedite window', 'The locals window', 'The watch window', 4),
(22, 2, 9, 'How can you print the object name associated with the last VB  error to the Immediate window?', 'Debug.Print Err.Number', 'Debug.Print Err.Source', 'Debug.Print Err.Description', 'Debug.Print Err.LastDLLError', 2),
(23, 2, 9, 'How can you print the object name associated with the last VB  error to the Immediate window?', 'Debug.Print Err.Number', 'Debug.Print Err.Source', 'Debug.Print Err.Description', 'Debug.Print Err.LastDLLError', 2),
(24, 2, 9, 'What function does the TabStop property on a command button perform?', 'It determines whether the button can get the focus', 'If set to False it disables the Tabindex property.', 'It determines the order in which the button will receive the focus', 'It determines if the access key swquence can be used', 1),
(25, 0, 10, 'You application creates an instance of a form. What is the first event that will be triggered in the from?', 'Load', 'GotFocus', 'Instance', 'Initialize', 4),
(26, 0, 10, 'Which of the following is Hungarian notation for a menu?', 'Menu', 'Men', 'mnu', 'MN', 3),
(27, 0, 10, 'You are ready to run your program to see if it works.Which key on your keyboard will start the program?', 'F2', 'F3', 'F4', 'F5', 4),
(28, 0, 10, 'Which of the following snippets of code will unload a form named frmFo0rm from memory?', 'Unload Form', 'Unload This', 'Unload Me', 'Unload', 3),
(29, 0, 10, 'You want the text in text box named txtMyText to read My Text.In which property will you place this string?', 'Caption', 'Text', 'String', 'None of the above', 2),
(30, 0, 11, 'how to use date( ) in mysql ?', 'now( )', 'today( )', 'date( )', 'time( )', 0),
(31, 0, 11, 'how to use date( ) in mysql ?', 'now( )', 'today( )', 'date( )', 'time( )', 0);

-- --------------------------------------------------------

--
-- Table structure for table `mst_result`
--

CREATE TABLE `mst_result` (
  `login` varchar(20) DEFAULT NULL,
  `test_id` int(5) DEFAULT NULL,
  `test_date` date DEFAULT NULL,
  `score` int(3) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mst_result`
--

INSERT INTO `mst_result` (`login`, `test_id`, `test_date`, `score`) VALUES
('raj', 8, '0000-00-00', 3),
('raj', 9, '0000-00-00', 3),
('raj', 8, '0000-00-00', 1),
('ashish', 10, '0000-00-00', 3),
('ashish', 9, '0000-00-00', 2),
('ashish', 10, '0000-00-00', 0),
('raj', 8, '0000-00-00', 0),
('ankur', 11, '0000-00-00', 0);

-- --------------------------------------------------------

--
-- Table structure for table `mst_subject`
--

CREATE TABLE `mst_subject` (
  `sub_id` int(5) NOT NULL,
  `sub_name` varchar(25) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mst_subject`
--

INSERT INTO `mst_subject` (`sub_id`, `sub_name`) VALUES
(1, 'VB'),
(2, 'Oracle'),
(3, 'Java'),
(4, 'PHP'),
(5, 'Computer Fundamental'),
(6, 'Networking'),
(7, 'mysql');

-- --------------------------------------------------------

--
-- Table structure for table `mst_test`
--

CREATE TABLE `mst_test` (
  `test_id` int(5) NOT NULL,
  `sub_id` int(5) DEFAULT NULL,
  `test_name` varchar(30) DEFAULT NULL,
  `total_que` varchar(15) DEFAULT NULL,
  `is_delete` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mst_test`
--

INSERT INTO `mst_test` (`test_id`, `sub_id`, `test_name`, `total_que`, `is_delete`) VALUES
(8, 1, 'VB Basic Test', '3', 0),
(9, 1, 'Essentials of VB', '5', 0),
(10, 1, 'Creating User Services', '5', 0),
(11, 7, 'function', '5', 0);

-- --------------------------------------------------------

--
-- Table structure for table `mst_user`
--

CREATE TABLE `mst_user` (
  `user_id` int(5) NOT NULL,
  `login` varchar(20) DEFAULT NULL,
  `pass` varchar(20) DEFAULT NULL,
  `username` varchar(30) DEFAULT NULL,
  `address` varchar(50) DEFAULT NULL,
  `city` varchar(15) DEFAULT NULL,
  `phone` int(10) DEFAULT NULL,
  `email` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mst_user`
--

INSERT INTO `mst_user` (`user_id`, `login`, `pass`, `username`, `address`, `city`, `phone`, `email`) VALUES
(1, 'raj', 'raj', 'Rajen', 'limbdi', 'limbdi', 9999, 'raj@yahoo.com'),
(12, 'ashish', 'shah', 'ashish', 'laskdjf', 'S\'nagar', 228585, 'ashish@yahoo.com'),
(14, 'Dhaval123', 'a', 'a', 'a', 'a', 0, 'dhaval@yahoo.com');

-- --------------------------------------------------------

--
-- Table structure for table `mst_useranswer`
--

CREATE TABLE `mst_useranswer` (
  `id` int(25) NOT NULL,
  `user_id` int(25) NOT NULL,
  `course_id` int(25) NOT NULL,
  `sess_id` varchar(80) DEFAULT NULL,
  `test_id` int(11) DEFAULT NULL,
  `que_des` varchar(200) DEFAULT NULL,
  `ans1` varchar(50) DEFAULT NULL,
  `ans2` varchar(50) DEFAULT NULL,
  `ans3` varchar(50) DEFAULT NULL,
  `ans4` varchar(50) DEFAULT NULL,
  `true_ans` int(11) DEFAULT NULL,
  `your_ans` int(11) DEFAULT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mst_useranswer`
--

INSERT INTO `mst_useranswer` (`id`, `user_id`, `course_id`, `sess_id`, `test_id`, `que_des`, `ans1`, `ans2`, `ans3`, `ans4`, `true_ans`, `your_ans`, `created_date`) VALUES
(9, 1, 8, 'kd3aahaga6u0aqa73dro8g91nkrjv28r', 8, 'What  Default Data Type ?', 'String', 'Variant', 'Integer', 'Boolear', 2, 1, '2018-01-27 10:20:52'),
(10, 1, 8, 'kd3aahaga6u0aqa73dro8g91nkrjv28r', 8, 'What is Default Form Border Style ?', 'Fixed Single', 'None', 'Sizeable', 'Fixed Diaglog', 3, 3, '2018-01-27 10:20:54'),
(11, 1, 8, 'kd3aahaga6u0aqa73dro8g91nkrjv28r', 8, 'Which is not type of Control ?', 'text', 'lable', 'checkbox', 'option button', 1, 4, '2018-01-27 10:20:56'),
(12, 1, 10, 'nkj4205ac8dm4ko40o3fi6k65thvm5kn', 10, 'You application creates an instance of a form. What is the first event that will be triggered in the from?', 'Load', 'GotFocus', 'Instance', 'Initialize', 4, 1, '2018-01-27 10:31:09'),
(13, 1, 10, 'nkj4205ac8dm4ko40o3fi6k65thvm5kn', 10, 'Which of the following is Hungarian notation for a menu?', 'Menu', 'Men', 'mnu', 'MN', 3, 1, '2018-01-27 10:31:12'),
(14, 1, 10, 'nkj4205ac8dm4ko40o3fi6k65thvm5kn', 10, 'You are ready to run your program to see if it works.Which key on your keyboard will start the program?', 'F2', 'F3', 'F4', 'F5', 4, 3, '2018-01-27 10:31:14'),
(15, 1, 10, 'nkj4205ac8dm4ko40o3fi6k65thvm5kn', 10, 'Which of the following snippets of code will unload a form named frmFo0rm from memory?', 'Unload Form', 'Unload This', 'Unload Me', 'Unload', 3, 4, '2018-01-27 10:31:15'),
(16, 1, 10, 'nkj4205ac8dm4ko40o3fi6k65thvm5kn', 10, 'You want the text in text box named txtMyText to read My Text.In which property will you place this string?', 'Caption', 'Text', 'String', 'None of the above', 2, 2, '2018-01-27 10:31:17');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_admin`
--

CREATE TABLE `tbl_admin` (
  `id` int(11) NOT NULL,
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `activation_token` int(11) NOT NULL,
  `activation_time` datetime NOT NULL,
  `keystring` varchar(255) NOT NULL,
  `isused` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_admin`
--

INSERT INTO `tbl_admin` (`id`, `first_name`, `last_name`, `email`, `password`, `activation_token`, `activation_time`, `keystring`, `isused`) VALUES
(1, 'Virendra', '', 'admin@cognigain.com', '123456', 0, '0000-00-00 00:00:00', '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_assessment`
--

CREATE TABLE `tbl_assessment` (
  `id` int(11) NOT NULL,
  `course_id` int(11) NOT NULL,
  `assessment_id` int(11) NOT NULL,
  `question` text NOT NULL,
  `answer_1_value` varchar(255) NOT NULL,
  `answer_1_flag` int(11) NOT NULL,
  `answer_2_value` varchar(255) NOT NULL,
  `answer_2_flag` int(11) NOT NULL,
  `answer_3_value` varchar(255) NOT NULL,
  `answer_3_flag` int(11) NOT NULL,
  `answer_4_value` varchar(255) NOT NULL,
  `answer_4_flag` int(11) NOT NULL,
  `is_delete` int(11) NOT NULL DEFAULT '0',
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_assessment`
--

INSERT INTO `tbl_assessment` (`id`, `course_id`, `assessment_id`, `question`, `answer_1_value`, `answer_1_flag`, `answer_2_value`, `answer_2_flag`, `answer_3_value`, `answer_3_flag`, `answer_4_value`, `answer_4_flag`, `is_delete`, `created_date`, `updated_date`) VALUES
(1, 2, 123, 'What is your name?', 'archit', 0, 'jainy', 0, 'arvin', 0, 'shyam', 1, 0, '2017-12-17 17:57:39', '0000-00-00 00:00:00'),
(2, 2, 111, 'This is question', 'ans1', 0, 'ans2', 0, 'ans3', 0, 'ans4', 0, 0, '2017-12-17 17:55:52', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_course`
--

CREATE TABLE `tbl_course` (
  `id` int(11) NOT NULL,
  `course_id` varchar(255) NOT NULL,
  `course_name` varchar(255) NOT NULL,
  `trainer` varchar(255) NOT NULL,
  `type_of_course` varchar(255) NOT NULL,
  `training_dates` date NOT NULL,
  `duration` date NOT NULL,
  `version` varchar(255) NOT NULL,
  `course_desc` text NOT NULL,
  `is_delete` int(11) NOT NULL COMMENT '1=deleted 0=not-deleted',
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_course`
--

INSERT INTO `tbl_course` (`id`, `course_id`, `course_name`, `trainer`, `type_of_course`, `training_dates`, `duration`, `version`, `course_desc`, `is_delete`, `created_date`, `updated_date`) VALUES
(1, 'C1', 'PGDHMM', 'jainy', 'IL', '2017-11-22', '2017-11-30', 'version1', 'demo', 0, '2017-12-31 07:03:17', '0000-00-00 00:00:00'),
(2, 'Course01', 'Project Management Test Course', 'Viren', 'ILT', '2017-11-28', '2017-11-26', '1.0', 'first test course', 0, '2017-12-15 16:59:11', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_topic`
--

CREATE TABLE `tbl_topic` (
  `id` int(11) NOT NULL,
  `topic_id` varchar(200) NOT NULL,
  `course_id` varchar(200) NOT NULL,
  `topic_name` varchar(255) NOT NULL,
  `topic_desc` text NOT NULL,
  `topic_url` varchar(255) NOT NULL,
  `is_delete` int(11) NOT NULL DEFAULT '0' COMMENT '1=deleted 0=not-deleted',
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_topic`
--

INSERT INTO `tbl_topic` (`id`, `topic_id`, `course_id`, `topic_name`, `topic_desc`, `topic_url`, `is_delete`, `created_date`, `updated_date`) VALUES
(1, 't1', 'C1', 'Topic First', '<p>asdadsadsad</p>\n', 'demo', 0, '2018-01-28 08:49:55', '0000-00-00 00:00:00'),
(2, 't2', 'C1', 'Topic Second', '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod\r\n                       tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,\r\n                       quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo\r\n                       consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse\r\n                       cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non\r\n                       proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\r\n                       <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod\r\n                       tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,\r\n                       quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo\r\n                       consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse\r\n                       cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non\r\n                       proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\r\n                       <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod\r\n                       tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,\r\n                       quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo\r\n                       consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse\r\n                       cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non\r\n                       proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\r\n                       <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod\r\n                       tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,\r\n                       quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo\r\n                       consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse\r\n                       cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non\r\n                       proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>', 'test', 0, '2018-01-28 08:50:01', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user`
--

CREATE TABLE `tbl_user` (
  `id` int(111) NOT NULL,
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `user_email` varchar(255) NOT NULL,
  `user_password` varchar(255) NOT NULL,
  `flag` tinyint(4) NOT NULL,
  `user_phone` varchar(50) NOT NULL,
  `user_notes` text NOT NULL,
  `user_profilePic` varchar(200) NOT NULL,
  `activation_token` varchar(255) NOT NULL,
  `activation_time` varchar(255) NOT NULL,
  `keystring` varchar(255) NOT NULL,
  `isused` int(10) NOT NULL,
  `deactivate` int(11) NOT NULL DEFAULT '0',
  `updatedtimestamp` datetime NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_user`
--

INSERT INTO `tbl_user` (`id`, `first_name`, `last_name`, `user_email`, `user_password`, `flag`, `user_phone`, `user_notes`, `user_profilePic`, `activation_token`, `activation_time`, `keystring`, `isused`, `deactivate`, `updatedtimestamp`, `created_date`, `updated_date`) VALUES
(1, 'jainendra', 'pal', 'jpal@evonix.co', 'e10adc3949ba59abbe56e057f20f883e', 1, '8329018261', 'Dummy text', '1_IMG_profile_pic.jpg', 'q009314eche8413l', '', 'ubzzts884akmhe7j', 0, 0, '2017-12-04 18:18:37', '2017-12-24 09:28:13', '0000-00-00 00:00:00'),
(2, 'jainendra', 'pal', 'sanjay@evonix.co', 'e10adc3949ba59abbe56e057f20f883e', 0, '8329018261', 'Dummy text', '', 'hq23bwo932fgqkgr', '', '', 0, 0, '0000-00-00 00:00:00', '2017-12-14 17:24:31', '0000-00-00 00:00:00'),
(10, 'jainendra', 'pal', 'archit@evonix.co', 'c33367701511b4f6020ec61ded352059', 0, '8329018261', 'Dummy text', '', 'k5htf1dwbw2sylrp', '', 'h7cbircv9c4r9wgn', 0, 0, '2017-12-12 17:18:06', '2017-12-14 17:24:31', '0000-00-00 00:00:00'),
(11, 'jainendra', 'pal', 'archit@evonix.co', 'c33367701511b4f6020ec61ded352059', 0, '8329018261', 'Dummy text', '', 'e8t3fx18tvk60w9m', '', 'h7cbircv9c4r9wgn', 0, 0, '2017-12-12 17:18:06', '2017-12-14 17:24:31', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user_assesments`
--

CREATE TABLE `tbl_user_assesments` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `test_id` int(11) NOT NULL,
  `flag` int(5) NOT NULL COMMENT '0=not_attempt, 1=attempt',
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user_course`
--

CREATE TABLE `tbl_user_course` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `course_id` int(11) NOT NULL,
  `course_amt` int(11) NOT NULL,
  `course_status` varchar(5) NOT NULL DEFAULT 'a' COMMENT 'a=inprogress,b=pause,c=complete',
  `course_runing_status` int(11) NOT NULL,
  `course_complete_status` int(100) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_user_course`
--

INSERT INTO `tbl_user_course` (`id`, `user_id`, `course_id`, `course_amt`, `course_status`, `course_runing_status`, `course_complete_status`, `created_date`) VALUES
(3, 1, 1, 5000, 'a', 0, 1, '2017-12-24 14:20:17'),
(4, 1, 2, 5000, 'c', 0, 1, '2017-12-26 09:32:26');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `mst_admin`
--
ALTER TABLE `mst_admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mst_question`
--
ALTER TABLE `mst_question`
  ADD PRIMARY KEY (`que_id`);

--
-- Indexes for table `mst_subject`
--
ALTER TABLE `mst_subject`
  ADD PRIMARY KEY (`sub_id`);

--
-- Indexes for table `mst_test`
--
ALTER TABLE `mst_test`
  ADD PRIMARY KEY (`test_id`);

--
-- Indexes for table `mst_user`
--
ALTER TABLE `mst_user`
  ADD PRIMARY KEY (`user_id`);

--
-- Indexes for table `mst_useranswer`
--
ALTER TABLE `mst_useranswer`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_admin`
--
ALTER TABLE `tbl_admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_assessment`
--
ALTER TABLE `tbl_assessment`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_course`
--
ALTER TABLE `tbl_course`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_topic`
--
ALTER TABLE `tbl_topic`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_user`
--
ALTER TABLE `tbl_user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_user_assesments`
--
ALTER TABLE `tbl_user_assesments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_user_course`
--
ALTER TABLE `tbl_user_course`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `mst_admin`
--
ALTER TABLE `mst_admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `mst_question`
--
ALTER TABLE `mst_question`
  MODIFY `que_id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT for table `mst_subject`
--
ALTER TABLE `mst_subject`
  MODIFY `sub_id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `mst_test`
--
ALTER TABLE `mst_test`
  MODIFY `test_id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `mst_user`
--
ALTER TABLE `mst_user`
  MODIFY `user_id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `mst_useranswer`
--
ALTER TABLE `mst_useranswer`
  MODIFY `id` int(25) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `tbl_admin`
--
ALTER TABLE `tbl_admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tbl_assessment`
--
ALTER TABLE `tbl_assessment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tbl_course`
--
ALTER TABLE `tbl_course`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tbl_topic`
--
ALTER TABLE `tbl_topic`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tbl_user`
--
ALTER TABLE `tbl_user`
  MODIFY `id` int(111) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `tbl_user_assesments`
--
ALTER TABLE `tbl_user_assesments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_user_course`
--
ALTER TABLE `tbl_user_course`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

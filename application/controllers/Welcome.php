<?php
ob_start();
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

   public function __construct()
   {
     parent::__construct();
     
       $this->load->library('session');
       $this->load->library('form_validation');
       $this->load->model('user_model');
       $this->load->library('email');
       $this->load->helper('url');
       $this->load->helper('security');
       $this->load->helper('cookie');

   }


	public function index()
	{

     //echo base_url(); exit;
        

        $this->load->view('frontend/allcss');
        $this->load->view('frontend/header');
	     	$this->load->view('frontend/index');
		    $this->load->view('frontend/alljs');
		    $this->load->view('frontend/pagewisejs/index.js');
		    $this->load->view('frontend/footer');
		
         
        // $this->load->view('frontend/index');

	}

// function for load login view

  public  function login()
  {

//    echo 123; exit;


       if($this->session->userdata('user_id'))

       {
          redirect(base_url().'welcome/dashboard');
       }

        $this->load->view('frontend/allcss');
        $this->load->view('frontend/header');
        $this->load->view('frontend/loginView');
        $this->load->view('frontend/alljs');
        $this->load->view('frontend/pagewisejs/index.js');
        $this->load->view('frontend/footer');

   

     

  }






// function for send mail

  public function sendTOmail($subject,$message,$username)
    {

  require 'vendor/autoload.php';

  $sendgrid = new SendGrid("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");
  $email    = new SendGrid\Email();

  $email->addTo($username)
  //->addCc(cc_email)
  ->setFrom('support@cognigain.com')
  ->setFromName("Cognigain | Learning Portal")
  ->setSubject($subject)
  ->setHtml($message);

  $sendgrid->send($email);



   }



	/*============== FUNCTION FOR KEY STRING==============*/

		public function generateRandomString($length = 16)
		{	
		 $characters   = 'abcdefghijklmnopqrstuvwxyz0123456789';
		 $randomString = '';
		 for ($i = 0; $i < $length; $i++) {
		  $randomString .= $characters[rand(0, strlen($characters) - 1)];
		}
		return $randomString;
		}


// function for load registration view

   public function register()
   {

    

        $this->load->view('frontend/allcss');
        $this->load->view('frontend/header');
        $this->load->view('frontend/registerationView');
        $this->load->view('frontend/alljs');
        $this->load->view('frontend/pagewisejs/index.js');
        $this->load->view('frontend/footer');





   } 



// function for signup 

public function signUp()
{
   // echo 123; exit;
    
   $_POST = json_decode(file_get_contents('php://input'),true);

  
  // echo "<pre>"; print_r($_POST); exit;

   if($this->input->post())
   {
 
      $this->form_validation->set_rules('fname','FirstName','trim|required|trim|xss_clean|strip_tags');


   $this->form_validation->set_rules('lname', 'LastName', 'trim|required|trim|xss_clean|strip_tags');

 //  $this->form_validation->set_rules('city', 'City', 'trim|required|trim|xss_clean|strip_tags|strip_tags');

   $this->form_validation->set_rules('email', 'Emailid', 'trim|required|trim|xss_clean|strip_tags|valid_email');

   $this->form_validation->set_rules('password', 'Password', 'trim|required|trim|xss_clean|strip_tags');

   $this->form_validation->set_rules('cpassword', 'Cpassword', 'trim|required|trim|xss_clean|strip_tags');
   $this->form_validation->set_rules('phonenumber', 'phonenumber', 'required');
  // $this->form_validation->set_rules('cpassword', 'Cpassword', 'trim|required|trim|xss_clean|strip_tags');

    if($this->form_validation->run()==TRUE)
    {
         //  echo "<pre>"; print_r($_POST); exit;
   


            $randomkeystring = $this->generateRandomString();
         $email = $this->input->post('email');

  //  $chkEmail =  $this->user_model->checkEmail($email);

                         // echo "<pre>"; print_r($chkEmail); exit;
      $info = array(

       'first_name'    => $this->input->post('fname'),
       'last_name'     =>  $this->input->post('lname'),
       'user_email'    => $this->input->post('email'),
       'user_password' =>md5($this->input->post('password')),
       'user_phone' =>  $this->input->post('phonenumber'),
       'user_notes'=>    $this->input->post('notes'),
       'activation_token' => $randomkeystring,
       'created_date'=>date("Y-m-d H:i:s")

       );



      //echo "<pre>"; print_r($info); exit;

      $response = $this->user_model->saveUserInfo($info);




      $url=base_url()."user/activate/".$randomkeystring;
      $fname = $this->input->post('fname');
      $lname = $this->input->post('lname');
      $password = $this->input->post('password');

      $fullname = $fname." ".$lname;



      $subject = "Thank you for registration";

      $message = "Dear ".$fullname."<br><br>";
      $message .="Greetings from Day Dreamer!<br><br>";
      $message .="Thank you for registration.<br><br>";



      $message .="Below are your login details .<br>";
      $message .="<b> Email</b> : ".$email." .<br> <b> Password</b>  :".$password.".<br>";



      $message .="<br />To activate your account, Please click the below link. <br>";
      $message .="<b> <a href=".$url."> Click Here.</a> </b><br><br>";



      $this->sendTOmail($subject,$message,$email);


      if ($response) {
       // echo 'if'; exit;
       $data = array('status' => '1');
       $this->session->set_flashdata('successmsg', 'Account activation link has been send on your email.Please activate your account.');
     }

     else 
     {  
      // echo 'else'; exit;
      $data = array('status' => '0');
      $this->session->set_flashdata('errormsg', '<div class="alert alert-warning alert-dismissable"><a type="button" class="close" data-dismiss="alert" aria-hidden="true">×</a>Error while submitting.</div>');
    }

  





}else{

  $data = array('status' => '2', 'error' => $this->form_validation->get_field_data(), 'postdata' => $this->input->post());
}

}else{

 $data = array('status' => '3');
 $this->session->set_flashdata('errormsg', '<div class="alert alert-warning alert-dismissable"><a type="button" class="close" data-dismiss="alert" aria-hidden="true">×</a>All fields are mandatory.</div>');

}



print_r(json_encode($data));



    }




 /*============ FUNCTION FOR UPDATE USER PROFILE ================*/
  
  public function updateUserProfile()
  {

    if(!$this->session->userdata('user_id'))
    {

     redirect(base_url().'welcome/index');

    }
      $_POST = json_decode(file_get_contents('php://input'), true);

    //  echo "<pre>"; print_r($_POST); exit;

      $user_id = $this->session->userdata('user_id');


      $info = array(

              'first_name' => $this->input->post('efname'), 
              'last_name' =>  $this->input->post('elname'),
              'user_phone' => $this->input->post('ephonenumber'),
              'user_notes' => $this->input->post('enotes')
           );
        
        $updateuserInfo = $this->user_model->updateUserProfile($user_id,$info);


        if($updateuserInfo)
        {

         $data = array('status' => '1');
       $this->session->set_flashdata('succmsg', 'Profile has been updated.');

        }
       
       print_r(json_encode($data));

  }


    /*========= FUCTION FOR SAVE PROFILE PICTURE ===================*/

   public function sbmtProfilePic()
   {

    define('UPLOAD_DIR', 'uploads/profilePic/');
          $_POST = json_decode(file_get_contents('php://input'), true);


        //   echo "<pre>"; print_r($_POST); exit;
          
          if ($this->input->post()) {
            
            $image_name = $_POST['userfile'];
            $img = $_POST['imgdata'];
         
         //  echo $image_name." ".$img; exit;
            $img = str_replace('data:image/png;base64,', '', $img);
         $img = str_replace(' ', '+', $img);
         $decoded = base64_decode($img);
       $uid=$this->session->userdata('user_id');
       $file = UPLOAD_DIR .$uid.'_'.$image_name;
       $file_name=$uid.'_'.$image_name;

        $success = file_put_contents($file, $decoded);

        if($success)
        {

           $upload = $this->user_model->saveProfilePic($file_name,$uid);


           if($upload)
             {
              
                 $data = array('status' =>'1');
                 $this->session->set_flashdata('succmsg','Profile picture has been uploaded successfully');
             }
        }

          }else{

           $data = array('status' => '2');
           $this->session->set_flashdata('errmsg','Please select profile picture properly');
    

          }

   print_r(json_encode($data));

   }



/*========Function for user login=========== */

public function checklogin() {

 $_POST = json_decode(file_get_contents('php://input'), true);

        //  print_r($_POST); exit;

 error_reporting(0);
 if ($this->input->post()) {
   $this->form_validation->set_rules('useremail', 'Email ID', 'trim|required|trim|xss_clean|strip_tags|valid_email');
   $this->form_validation->set_rules('userPassword', 'Password', 'trim|required|trim|xss_clean|strip_tags');
   //$this->form_validation->set_rules('remember','Rememerme','');




   if ($this->form_validation->run() == TRUE) {

    $emailaddress = $this->input->post('useremail');
    $password = $this->input->post('userPassword');


              $response = $this->user_model->check_user_login($emailaddress, $password);

              if ($response) {

              $user_id = $response->id;

              $userLogedData = array(
              'user_id'    => $response->id,
              'user_email' => $response->user_email,
              'f_name'     => $response->first_name,
              'l_name'     => $response->last_name,
              /*'profilePic' => $user_profileData->user_profilePic,*/
              'user_login' => true
              );

             // echo "<pre>"; print_r($userLogedData); exit; 

              $this->session->set_userdata($userLogedData);
            

        // echo $this->session->userdata('f_name'); exit;

               $data = array('status' => '1');
            }
              else {
                        $data = array('status' => '0');
               $data = array('status' => '0', 'message' => '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>Wrong username or password combination !</div>');
              }
  } else {
  $data = array('status' => '2', 'error' => $this->form_validation->get_field_data(), 'postdata' => $this->input->post());
}
          //print_r($this->form_validation->get_field_data()); exit;
         //print_r($data);        
}
else {
 $data = array('status' => '3');
}
      
print_r(json_encode($data));
      //echo json_encode($data);
}




  /*====================FUNCTION FOR CHECK DUPLICATE EMAILID ===============*/

      public function checkEmail()
     {

       $email = $this->input->post('email');

      
       $checkEmail =   $this->user_model->findEmail($email);

      //echo "<pre>"; print_r($checkEmail); exit;
    if($checkEmail != null)
    {

     echo 0;


    }else{

      echo 1;
    }

     }


/* function for search result */

 public function  searchResult()
 {
  //echo "<pre>"; print_r($_POST); exit;

   $courseName = $this->input->post('cName');
   $trainerName = $this->input->post('tname');
   $date        = $this->input->post('tdate');

   if(isset($courseName) || isset($trainerName) || isset($date) )
   {
    //echo $courseName."  ".$trainerName."  ".$date; exit;


     $data['searchedResult'] =$this->user_model->searchResult($courseName,$trainerName,$date);

   }
   $this->load->view('frontend/allcss');
    $this->load->view('frontend/header');
    $this->load->view('frontend/searchView',$data);
  
    $this->load->view('frontend/alljs');
    $this->load->view('frontend/pagewisejs/search.js');
    $this->load->view('frontend/footer');

   
 }



/**************** FUNCTION FOR COURSE DETAIL=================*/
 public function courseDetail($id)
 {
    //echo $id; exit;

    echo "Course description will come here";


 }




// function for check emailid while reset password

/*=============== FUNCTION FOR FORGOT PASSWORD =============*/
public function check_forgot_password()
{
  
 $_POST = json_decode(file_get_contents('php://input'), true);


       //$_POST = json_decode(file_get_contents('php://input'), true);
  
// echo "<pre>"; print_r($_POST); exit;

 if($this->input->post()) {

   $this->form_validation->set_rules('email', 'Email ID', 'trim|required|trim|xss_clean|strip_tags|valid_email');
   if ($this->form_validation->run() == TRUE) {
    $emailaddress = $this->input->post('email'); 
    $randomkeystring = $this->generateRandomString();
    $url = base_url().'Welcome/';
    $response = $this->user_model->check_admin_forgot_password($emailaddress);
    if ($response) {
              //echo '<pre>';print_r($response);exit;
        //$data = array('status' => '1');
        //$updatepassword = $this->admin_model->update_password($emailaddress,$randomkeystring);
      $updatekeystring = $this->user_model->update_keystring($emailaddress,$randomkeystring);

        //send mail
      require 'vendor/autoload.php';

      $sendgrid = new SendGrid("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");
      $email = new SendGrid\Email();

      $email->addTo($emailaddress)
      ->setFrom('support@cognigain.com')
      ->setFromName('Cognigain')
      ->setSubject("Here's the link to reset your password")
      ->setHtml("You recently requested a password reset.<br />To change your password<br /><a href='".$url."resetpassword/$randomkeystring'>Click here</a><br />The link will expire in 12 hours, so be sure to use it right away.<br /><br /> <br />Regards, <br />Support Team.");

      if($sendgrid->send($email)) {
        $data = array('status' => '1', 'message' => '<div class="alert alert-success">An email has been sent to '.$emailaddress.'. Please click on the link provided in the mail to reset your password.</div>');
      } 
      else {
       $data = array('status' => '4', 'message' => '<div class="alert alert-danger">Error in sending Email.</div>');
     }

   }
   else {
    $data = array('status' => '0', 'message' => '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>The email ID you have entered does not exist !.</div>');
  }
} else {
  $data = array('status' => '2', 'error' => $this->form_validation->get_field_data(), 'postdata' => $this->input->post());
}       
}
else {
  $data = array('status' => '3');
}
print_r(json_encode($data));



}

 


public function check_reset_password() {
  $_POST = json_decode(file_get_contents('php://input'), true);
        //print_r($_POST); exit;
  if ($this->input->post()) {
   $this->form_validation->set_rules('npassword', 'New Password', 'trim|required|trim|xss_clean|strip_tags');
   $this->form_validation->set_rules('cpassword', 'Confirm Password', 'trim|required|trim|xss_clean|strip_tags|matches[npassword]');
   if ($this->form_validation->run() == TRUE) {
    $url = base_url();
    $emailaddress = $this->input->post('emailaddress');
    $npassword = $this->input->post('npassword');
    $updatepassword = $this->user_model->update_password($emailaddress,$npassword);
            //$response = $this->admin_model->check_user_login($emailaddress, $npassword);
    if ($updatepassword) {
      $data = array('status' => '1', 'message' => '<div class="alert alert-success">You have successfully changed your password. Please sign in with your new password.<br><a href="'.$url.'welcome/login" class="btn btn-twitter" style="padding: 2px 8px;">Sign In</a></div>');
    }
    else {
        //$data = array('status' => '0');
      $data = array('status' => '0', 'message' => '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>Oops... Something went wrong !.</div>');
    }
  } else {
    $data = array('status' => '2', 'error' => $this->form_validation->get_field_data(), 'postdata' => $this->input->post());
  }
       //print_r($data);        
}
else {
  $data = array('status' => '3');
}
    //print_r($data); exit;
print_r(json_encode($data));
    //echo json_encode($data);
}








/*============= FUNCTION FOR ACTIVATE SIGNUP ACCOUNT =============*/

public function activate($token)

{

  $actvntm = date("Y-m-d H:i:s");

  $data = array(

    'flag'=>1,

    'activation_time'  => $actvntm,
    );

  $status = $this->user_model->activateLogin($data,$token);

  if($status)
  {
    $this->session->set_flashdata('succmsg',"Your email id has been verified. Login with your credentials");

    redirect(base_url().'user');

  }


}





// function for user courseCatalog 

public function courseCatalog()

{
  if(!$this->session->userdata('user_id'))
    {

     redirect(base_url().'welcome/index');

   }
     // echo 123; exit;
     
     if($this->session->userdata('user_id'))

     {
        
        $user_id = $this->session->userdata('user_id');
        $data['enrolledCourses'] = $this->user_model->checkEnrolledCourse($user_id);


     }

    $data['courses']=$this->user_model->getAllCourses();
           // $this->load->view('frontend/allcss');
       $this->load->view('frontend/allcss');
       $this->load->view('frontend/header');
       $this->load->view('frontend/courseCatalog',$data);
       $this->load->view('frontend/alljs');
       $this->load->view('frontend/footer');


}


/*function for load dashboard*/


public function dashboard()

{  
   if(!$this->session->userdata('user_id'))
    {

     redirect(base_url().'welcome/index');

   }
    
    $user_id = $this->session->userdata('user_id');

    $data['userDashboardData'] = $this->user_model->getUserDashboardData($user_id);
  //  $data['showResult']     = $this->user_model->getUserAssesmentResult($user_id);
 

   $this->load->view('frontend/allcss');
   $this->load->view('frontend/header');
   $this->load->view('frontend/dashboard',$data);
   $this->load->view('frontend/footer');
   $this->load->view('frontend/alljs');
   $this->load->view('frontend/pagewisejs/dashboard.js');
   


}


/*function for load view for enrolled courses*/

public function enrolledCourses()
{

   if(!$this->session->userdata('user_id'))
    {

     redirect(base_url().'welcome/index');

     }
     
      $user_id = $this->session->userdata('user_id');

    $data['userEnrolledCourses'] = $this->user_model->getUserEnrolledCourses($user_id);
    

   $this->load->view('frontend/allcss');
   $this->load->view('frontend/header');
   $this->load->view('frontend/enrolledCourses',$data);
   $this->load->view('frontend/alljs');
   $this->load->view('frontend/pagewisejs/enrolledCourses.js');
   $this->load->view('frontend/footer');

   
}


/*============= function for load view myprofile ==============*/

public function myProfile()
{


   if(!$this->session->userdata('user_id'))
    {

     redirect(base_url().'welcome/index');

   } 
     $user_id = $this->session->userdata('user_id'); 

    $data['userProfileData'] = $this->user_model->getUserProfileData($user_id);

   // echo "<pre>"; print_r($data['userProfileData']); exit;

   $this->load->view('frontend/allcss');
   $this->load->view('frontend/header');
   $this->load->view('frontend/myProfile',$data);
   $this->load->view('frontend/alljs');
    $this->load->view('frontend/pagewisejs/myProfile.js');
   $this->load->view('frontend/footer');

}



/*============ FUNCTION FOR RESET PASSWORD ===========*/


function resetpassword($randomkeystring='')
{
  $keystring['data'] = $this->user_model->check_randomkey_string($randomkeystring);
  if(empty($keystring['data']))
  {
    $rediect_path = base_url().'Welcome';
    redirect($redirect_path, 'refresh');
  }

  $this->load->view('frontend/allcss');
  $this->load->view('frontend/header');
  $this->load->view('frontend/resetpassword',$keystring);
  
  $this->load->view('frontend/alljs');
  $this->load->view('frontend/pagewisejs/resetpassword.js');
  $this->load->view('frontend/footer');
  

}
// function for load search view
public function search()
{

    $this->load->view('frontend/allcss');
    $this->load->view('frontend/header');
    $this->load->view('frontend/searchView');
  
    $this->load->view('frontend/alljs');
    $this->load->view('frontend/pagewisejs/search.js');
    $this->load->view('frontend/footer');
    

}

/*function for load view learning history*/

public function learningHistory()
{

   if(!$this->session->userdata('user_id'))
    {

     redirect(base_url().'welcome/index');

   }
     
     $user_id = $this->session->userdata('user_id');    

   $data['userLearningHistory'] = $this->user_model->getUserLearningHistory($user_id);


    $this->load->view('frontend/allcss');
    $this->load->view('frontend/header');
    $this->load->view('frontend/learningHistory',$data);
    $this->load->view('frontend/alljs');
    $this->load->view('frontend/pagewisejs/learningHistory.js');
    $this->load->view('frontend/footer');


}



/*function for make course payment*/

public function makepayment()
{


   if(!$this->session->userdata('user_id'))
    {

     redirect(base_url().'welcome/index');

   }


     // echo "<pre>"; print_r($_POST);

   $user_id =$this->session->userdata('user_id');

   
   $courseInfo = array(

        'user_id' => $user_id,
        'course_id' => $this->input->post('course_id'),
        'course_amt' => $this->input->post('course_amt') 
         );

      
      $savaCourse = $this->user_model->saveUserCourse($courseInfo);


      if($savaCourse)
      {

        redirect(base_url('welcome/enrolledCourses'));
 

      }

 

}



/*============= function for change status =============*/

public function changeStatus($cId)
{
 
// echo $id; exit;

  $user_id = $this->session->userdata('user_id');

   $data = array(
       
         'course_complete_status' =>1
       
         );
   
    
       $this->session->set_userdata('cId',$cId);

   $changeStatus = $this->user_model->changeCourseSatus($data,$user_id,$cId);

   $data['courseName'] = $this->user_model->getCourseName($cId);
   /*if($changeStatus)
   {
 
     
     $this->session->set_flashdata('succmsg','Status has been changed');
     redirect(base_url('welcome/enrolledCourses'));

 
   }*/
   
    // $data['courseTopics']= $this->user_model->getCourseTopics($user_id,$cId);
    $data['courseTopics'] = $this->user_model->getTopicsDescriptions($cId);
    $data['assesments'] = $this->user_model->getCourseAssesments($cId);
     
   // echo "<pre>"; print_r($data['courseTopics']); exit;

     
    $this->load->view('frontend/allcss');
    $this->load->view('frontend/header');
    $this->load->view('frontend/learnCourse',$data);
    $this->load->view('frontend/alljs');
    //
    $this->load->view('frontend/footer');
    $this->load->view('frontend/pagewisejs/learningCourse.js');



}










/*=============FUNCTION FOR LOG OUT ==============*/

public  function logout()
{


  if(!$this->session->userdata('user_id'))
  {

   redirect(base_url().'welcome/index');

 }
 


 if($this->session->userdata('user_login'))
 {


  $this->session->unset_userdata('user_login');
  $this->session->unset_userdata('user_email');
  $this->session->unset_userdata('f_name');
  $this->session->unset_userdata('l_name');
  $this->session->unset_userdata('user_id');

            //  $this->session->sess_destroy();

  redirect(base_url().'welcome/index');
}

}


public function test()
{

$this->load->view('frontend/test');



}


/*=================== play course ================*/

 public function playCourse()

 {

   $course_id = $this->input->post('id');

   //echo $course_id; exit;

   $courseData = $this->user_model->getCourseData($id);


 

 }


/* function for assesments*/

public function assesment()
{

   if(!$this->session->userdata('user_id'))
  {
   redirect(base_url().'welcome/index');
   }

 $user_id = $this->session->userdata('user_id');

   $data['userCourses'] = $this->user_model->getUserEnrolledCoursesForAssesment($user_id);

    $this->load->view('frontend/allcss');
    $this->load->view('frontend/header');
    $this->load->view('frontend/courseList',$data);
    $this->load->view('frontend/alljs');
    
    $this->load->view('frontend/footer');
    $this->load->view('frontend/pagewisejs/courseList.js');



}

/*function for selsect assements */

public function selectAssesments($sid)
{

   //echo $sid; exit;
   if(!$this->session->userdata('user_id'))
   {

   redirect(base_url().'welcome/index');
  
   }

   $user_id = $this->session->userdata('user_id');
   
   $data['assements'] =  $this->user_model->getSubjectAssesments($sid);

   $data['assesments_status'] = $this->user_model->getAssesmentsStatus($user_id);

     

     
   //  echo  "<pre>"; print_r($assements); exit;
     
    $this->load->view('frontend/allcss');
    $this->load->view('frontend/header');
    $this->load->view('frontend/assesmentsList',$data);
    $this->load->view('frontend/alljs');
    
    $this->load->view('frontend/footer');
    $this->load->view('frontend/pagewisejs/selectAssesments.js');
     
    
}
//,$tid='',$question_id=''

public function startAssesments($sid)
{
  
   if(!$this->session->userdata('user_id'))
   {

   redirect(base_url().'welcome/index');
  
   }

  $data['sid']=$sid;
  //$data['tid']=$tid;
  //$data['ques_id']=$question_id;
  $user_id = $this->session->userdata('user_id');
  //$this->session->set_userdata('tid',$tid);
  $this->session->set_userdata('sid',$sid);



  if(!$this->session->userdata('qn'))
  {

   //$deleteUserAns = $this->user_model->delUserAns();
   
   $data['qn']=$this->session->set_userdata('qn',0);
   $this->session->set_userdata('trueans',0);
  
  }

  $data['courseName'] =  $this->user_model->getCourseNameforAssesment($sid,$user_id);  

  //print_r($data['courseName']); exit;

 $this->load->view('frontend/quiz',$data);
 $this->load->view('frontend/footer');


  }

/*function for save answer */
    public  function userAnswer()
    {
      
       date_default_timezone_set("Asia/Kolkata");

    //echo "<pre>"; print_r($_POST); exit();
     if(isset($_POST))
    {
      
       $tid = $this->input->post('testid'); 
       //echo $tid; exit;
       $question_id = $this->input->post('ques_id');
       $quesName= $this->input->post('questionsName');
       $marks=0;

       
       $userAns = $this->input->post('ans');
       $ans1 =   $this->input->post('ans1');
       $ans2 =   $this->input->post('ans2');
       $ans3 = $this->input->post('ans3');
       $ans4 = $this->input->post('ans4');
       $correctAns = $this->input->post('correctAns');
       $subject_id = $this->input->post('subjectId');
       $attempt_count = 0;
       //$rs=mysqli_query("select * from mst_question where test_id=$tid",$cn);
 
       if($userAns==$correctAns)
       {

           $marks=10;

       }
        
        $user_id = $this->session->userdata('user_id');

       //$rs = $this->user_model->getQuestionFroMst($tid);
       $correctAns= $this->user_model->getCorrectAns($question_id);

       $totalQues = $this->user_model->getTotalQuestions($subject_id);
        $tq=$totalQues->noOfQues;
         // /echo $tq; exit;
       $totalAnswerdQues = $this->user_model->getUserAnwerdsQues($subject_id,$user_id);
        $taq= $totalAnswerdQues->ansQues;
        // echo $tq.'tq   '.'   '.$taq.'taq';   exit;
        
        if($tq==$taq)
        {
            
           $attemptData = array(
                    'user_id' =>$user_id,
                    'course_id' =>$subject_id
                    );

           $this->user_model->saveUserAtteptAssem($attemptData);

        }
       

             $ansArray1 = array(
                       'ques_id' => $question_id,
                       'user_id'=> $user_id,
                       'course_id' =>$subject_id,
                       'test_id' =>$tid,
                       'attempt_count' =>$attempt_count,
                       'user_ans' =>$userAns,
                       'marks' =>  $marks,
                       'created_date' => date('Y-m-d h:i:s')
                     );  

         $check = $this->user_model->checkUserAns($question_id,$user_id,$subject_id);

         // /echo "<pre>"; print_r($check); exit;

         if(!empty($check))
         {
            $deleteOldAns = $this->user_model->deleteOldAns($question_id,$user_id,$subject_id);

                   
               $saveUserAns = $this->user_model->saveUserAns($ansArray1);

         }else{

             $saveUserAns = $this->user_model->saveUserAns($ansArray1);
         }

      
     /* $savedUserAns = $this->user_model->getSavedUserAns($subject_id,$user_id);*/

     
         
          //  $saveUserAns = $this->user_model->saveUserAns($ansArray1);

            $qn = $this->session->userdata('qn'+1);
         
            $this->session->set_userdata('qn',$this->session->userdata('qn')+1);

            //$qn=$qn+1;
                   
            redirect(base_url().'welcome/startAssesments/'.$subject_id.'/'.$question_id);

        
    }



    }

  /*function for save start assesments*/
  
  public function setStartAssesments()
  {
     
     //echo "<pre>"; print_r($_POST); exit;
   if(isset($_POST))
    
    $user_id = $this->input->post('user_id');
    $test_id = $this->input->post('test_id');

    $saveData = array(
           
           'user_id '=> $user_id,
           'test_id' => $test_id,
           'flag' => 1
  
          );            
          

          $setStatus = $this->user_model->saveAssesmentsSatus($saveData); 
          
  }

  // function for view result

  public function viewResult()
  {
   
    if(!$this->session->userdata('user_id'))
   {

   redirect(base_url().'welcome/index');
  
   }




     $user_id = $this->session->userdata('user_id');
     $course_id = $this->session->userdata('cId');

    // echo $course_id.'  '.$user_id; exit;
     $assesmentsResult = $this->user_model->getStudentResult($user_id,$course_id);

      echo "<pre>"; print_r($assesmentsResult); exit();


  }

  public function testView()
  {
   
   
    $this->load->view('frontend/allcss');
    $this->load->view('frontend/header');
    $this->load->view('frontend/testResultView');
    $this->load->view('frontend/alljs');
    
    $this->load->view('frontend/footer');
   

  }  



}
?>
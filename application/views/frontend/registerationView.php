<div class="row" style="padding-top: 40px;">
    <div class="col-md-4">
<p style="padding-left:25px;  "><span>Home</span> > <span>Register New Student</span></p> 
</div>
 <div class="col-md-8 col-md-push-4">
  <div class="col-md-2">
      <a href="<?php echo base_url(); ?>welcome/courseCatalog"><p>Course Catalog</p></a>
  </div>
  <div class="col-md-1">
      <a href="<?php echo base_url(); ?>welcome/search"><p>Search</p></a>
  </div>
  <div class="col-md-1">
      <a href="#"><p>Help</p></a>
  </div>
  <?php if($this->session->userdata('user_id')){ ?>
  <div class="col-md-2">
      <a href="<?php echo base_url(); ?>welcome/logout"><p>Log Off</p></a>
  </div> 
  <?php }?>
</div>
<?php error_reporting(0); ?>
<div class="container">

  <div class="row" style="padding-top: 45px;">
  

  		<div class="panel-body">

  			<div class="col-md-12">

  		    <div class="col-md-4">
           
  		    	<!-- for file upload code -->


          <div class="profileimg">
          
        <!--  <a  editprofileimg data ="" data-toggle="modal" data-target="#myModal" href="javascript:void(0);" ><i class="fa fa-edit"></i></a>
 -->      
                       
                    
          <img src="http://localhost/cognilatest\uploads\profilePic\Desert.jpg" class="img-responsive img-thumbnail img-thumbnail" style="max-width: 60%">
          <a  class="editprofileimg" data ="" data-toggle="modal" data-target="#myModal" href="javascript:void(0);" ><i class="fa fa-edit"></i></a>
          <h3>Stud Id</h3>
          <p>Jhon</p>

        </div>




  		    </div>

  		    <div class="col-md-8">
  		    	<div class="col-md-3">
  		    		<div class="form-group">
  		    		<label class="notes">First Name</label>
  		    	</div>
  		    	<div class="form-group">
                   <label class="notes">Last Name</label>
               </div>
               <div class="form-group">
                   <label class="notes">Password</label>
               </div>
               <div class="form-group">
                   <label class="notes">Confirm Password</label>
               </div>
               <div class="form-group">
                   <label class="notes">Phone Number</label>
               </div>
               <div class="form-group">
                   <label class="notes">Email Address</label>
               </div>
               <div class="form-group">
                   <label class="notes">Notes</label>
               </div>

  		    	</div>
             <form name="userSignup" ng-submit="submitFormsignUp()" novalidate class="errorsalign">
  		    	<div class="col-md-5">
            
  		    		<div class="form-group">
  		    			<input type="text" class="form-control" placeholder="First Name" ng-model="user.fname" name="fname"  required>
                  <span ng-show="submitted && userSignup.fname.$invalid" class="help-block has-error ng-hide warnig">Please Enter First Name</span> 
             <span class="help-block has-error ng-hide warnig" ng-show="fnameError">{{fnameError}}</span> 

  		    		</div>
                <div class="form-group">
  		    			<input type="text" class="form-control" placeholder="Last Name" ng-model="user.lname" name="lname"  required>
                  <span ng-show="submitted && userSignup.lname.$invalid" class="help-block has-error ng-hide warnig">Please Enter Last Name</span> 
             <span class="help-block has-error ng-hide warnig" ng-show="fnameError">{{lnameError}}</span>

  		    		</div>
                      <div class="form-group">
  		    			<input type="password" class="form-control" name="password" ng-model="user.password" placeholder="Password" ng-minlength="6" ng-maxlength="20" required>
             
            <span ng-show="submitted && userSignup.password.$invalid" class="help-block has-error ng-hide warnig">Please Enter Password (Min six charecters)</span>
            <span class="help-block has-error ng-hide warnig" ng-show="passwordError">{{passwordError}}</span>
             </div>
                     <div class="form-group">
  		    			 <input type="password" class="form-control" name='cpassword' ng-model='user.cpassword' placeholder="Validate Password" required>
            <span ng-show="submitted && userSignup.cpassword.$error.required"  class="help-block has-error ng-hide warnig">validate password is required.</span>
            <span ng-show="errorcpassword" class="help-block has-error ng-hide warnig">{{errorcpassword}}</span>
            <span ng-show="user.password !== user.cpassword" class="help-block has-error ng-hide warnig">Passwords have to match!</span>

  		    		</div>
                       <div class="form-group">
  		    			<input type="text" class="form-control" name="phonenumber" ng-model='user.phonenumber' placeholder="Phone Number" required>
                <span ng-show="submitted && userSignup.phonenumber.$invalid" class="help-block has-error ng-hide warnig">Please Enter Email </span> 
             <span class="help-block has-error ng-hide warnig" ng-show="phonenumberError">{{emailError}}</span>
  		    		</div>
              <div class="form-group">
  		    			<input class="form-control" id="emailid" placeholder="Email Id"  type="email"  ng-model="user.email" name="email" autocomplete="off"  required="" />
                 <span id="email_status" style="color:red;"></span>
                 <span ng-show="submitted && userSignup.email.$invalid" class="help-block has-error ng-hide warnig">Please Enter Email </span> 
             <span class="help-block has-error ng-hide warnig" ng-show="emaileError">{{emailError}}</span>
  		    		</div>
  		    		<div class="form-group">
  		    			<textarea style="min-width: 100px;min-height:75px;width:403px;height:86px;" name="notes" ng-model="user.notes" placeholder="note"></textarea>
  		    		</div>
  		    		</div>
            <div class="col-xs-9 pull-right">
                          <!-- <button class="btn btn-sm btn-login btn-block" type="submit" ng-click="submitted = true">Submit</button> -->
                          <!-- <a href="<?php echo base_url(); ?>welcome/dashboard"><button type="button" type="submit" class="btn btn-primary" ng-click="submitted = true">Create</button></a> -->
                         <button class="btn btn-primary" type="submit" ng-click="submitted = true">Create</button>
                          <span class="help-block has-error warnig" ng-show="IsMatch">Password does not match Confirm Password.</span>

                          
                        </div>
                      
  		    	</div>

         </form>

  		    </div>
            


					

  		</div>
  		


  	
  	

  </div>

	
</div>


 <!--  modal open for change profile picture -->
                           <form class="form-horizontal bucket-form" id="cropimage"  name="profilePic" ng-submit="submitformProfile()" novalidate>

                              
                                    <div class="modal fade" id="myModal" role="dialog" style="background-color: rgba(0, 0, 0, 0.5);">
                                        <div class="modal-dialog">

                                          <!-- Modal content-->
                                          <div class="modal-content">
                                            <div class="modal-header">
                                              <button type="button" class="close" data-dismiss="modal">&times;</button>
                                              <h4 class="modal-title">Change Profile Picture</h4>
                                          </div>
                                      <div class="modal-body">
                                       <div class="form-group">
                              
                                         <div class="col-sm-10">
                                  <div class="image-editor">
                          <input type="file" accept="image*/" ng-model="brand.userfile" id="FileUpload1" class="cropit-image-input"><br>
                          <span style="color:#a94442;">Profie picture must be minimum width 200px and minimum height 200px.</span>

                           
                          <div class="cropit-preview"></div>
                          <div class="image-size-label">
                              Resize image
                          </div>
                          <input type="range" class="cropit-image-zoom-input">
                        

                          <button class="export">Upload</button>
                          <span id="succ" style="color: green;" ></span>
                         </div>
                         <input type="hidden" class="form-control" id="doc_pic" name="doc_pic" ng-model="brand.doc_pic" required/>
                        
                         
                         <span ng-show="submittedd && profilePic.doc_pic.$invalid" class="help-block has-error ng-hide warnig">Please upload profile picture</span>
                            <span class="help-block has-error ng-hide warnig" style="color: red;" ng-show="doc_picError">{{doc_picError}}</span>
                              </div>
                          </div>

                                  <div class="row">
                       <div class="col-md-12">
                          <button type="submit" ng-click="submittedd = true" class="btn btn-info pull-right btn-sm"><strong> Update <i class="fa fa-arrow-right"></i></strong></button>
                      </div>
                  </div>
                                  </div>
                                  </form>
              <div class="modal-footer">
                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              </div>
          </div>
          
      </div>
  </div>

<!--  modal close-->
  <section id="inner-wrapper">
  <p style="padding-left:25px; padding-top: 40px; "><span>Home</span> > <span>Login</span></p> 

  <div class="container">
  <div class="row">
  	 <div class="panel">
                  <div class="panel-body">
     <div class="col-md-12">
      <div class="col-md-2">
        <div class="form-group">
              <label class="">User Name</label>
            </div>
            <div class="form-group">
                   <label style="padding-top: 5px;">Password</label>
               </div>

      </div>



     	 <div class="col-md-4">
        <div id="message"></div>
     	 	 <form name="userLogin" ng-submit="submitLoginForm()" novalidate>
             
             <div class="form-group">
            <!--  <label class="control-label">Email Id </label> -->
     	 	 <input class="form-control" placeholder="Email Id"  type="email"  ng-model="login.useremail" name="useremail" autocomplete="off"  required="" />
              
                     <span ng-show="submittedd && userLogin.useremail.$invalid" class="help-block has-error ng-hide warnig">Please Enter Email </span> 
                    <span class="help-block has-error ng-hide warnig" ng-show="useremaileError">{{useremailError}}</span>
  			</div>

  			<div class="form-group">
  			 <!--  <label for="pwd">Password:</label> -->
  			  <input class="form-control" placeholder="Password" type="password"  ng-model="login.userPassword" name="userPassword"  required="" />
             
                         <span ng-show="submittedd && userLogin.userPassword.$invalid" class="help-block has-error ng-hide warnig">Please Enter Password </span> 
               <span class="help-block has-error ng-hide warnig" ng-show="userPasswordError">{{userPasswordError}}</span>
  			</div> 
               
               			<div class="">
                           <!--  <button type="submit" class="btn btn-green btn-block btn-flat" >Sign In</button> -->
                           <button class="btn btn-primary btn-lg btn-login btn-block" type="submit" ng-click="submittedd = true">Login</button>
                        </div>
                        <span> <a href="javascript:void(0);" data-target="#myModal" data-toggle="modal" style="padding-left: 217px;">Forgot Password</a></span>
          </form>

             <p>New User? <a href="<?php echo base_url(); ?>welcome/register">Register</a></p>

     	 </div>





<!-- cross-site request forgery (CSRF) -->
     	 <div class="col-md-6">



       <div class="vl"></div> 
     	 	

     	 </div>
     	

     </div>



  </div>

  </div>

  </div>

   <div class="modal fade" id="login" role="dialog">
        <div class="modal-dialog modal-sm">
        
          <!-- Modal content no 1-->
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h4 class="modal-title text-center form-title">Reset Password</h4>
            </div>
            <div class="modal-body padtrbl">

              <div class="login-box-body">
                

                <div class="form-group">
                  <form name="userForget" ng-submit="submitForgotForm()" novalidate>

                   <div class="form-group has-feedback">
                       <!--  <input class="form-control" placeholder="Email"   type="email" autocomplete="off" name="useremail" ng-model="login.useremail" /> --> 
             
                         <input class="form-control" placeholder="Email Id"  type="email" ng-model="forgot.useremail" name="useremail" autocomplete="off" id="email" required="" />
              
                        <span ng-show="fsubmittedd && userForget.useremail.$invalid" class="help-block has-error ng-hide warnig">Please Enter Email </span> 
                    <span class="help-block has-error ng-hide warnig" ng-show="useremaileError">{{useremailError}}</span>


                    </div>
                    
                    <div class="row">
                        
                        <div class="col-xs-12">
                           <!--  <button type="submit" class="btn btn-green btn-block btn-flat" >Sign In</button> -->
                           <button class="btn btn-lg btn-login btn-block" type="submit" ng-click="fsubmittedd = true">Submit</button>
                        </div>
                    </div>
                  </form>

                </div>
              </div>
            </div>
          </div>

        </div>
      </div>


<!-- new modal start -->

  <form class="form-signin" name="userForgotPassword" ng-submit="submitForgotPasswordForm()" novalidate>
              <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="myModal" class="modal fade">
                  <div class="modal-dialog" style="z-index:800;">
                      <div class="modal-content">
                          <div class="modal-header">
                              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                              <h4 class="modal-title">Forgot Password ?</h4>
                          </div>
                          <div class="modal-body">
                          <div id="message2"></div>
                              <p>Enter your e-mail address below to reset your password.</p>
                              <input type="email" id="email" name="email" placeholder="Email ID" class="form-control placeholder-no-fix" ng-model="forgotpassword.email" required>
                              <span ng-show="forgotpasswordsubmitted && userForgotPassword.email.$invalid" class="help-block has-error warnig">Valid Email ID is required.</span>
          <span class="help-block has-error ng-hide warnig" ng-show="emailError">{{emailError}}</span>
    
                          </div>
                          <div class="modal-footer">
                              <button data-dismiss="modal" class="btn btn-default" type="button">Cancel</button>
                              <button class="btn btn-theme" type="submit" ng-click="forgotpasswordsubmitted = true">Submit</button>
                          </div>
                      </div>
                  </div>
              </div>
    
          </form>
<!-- new modal end -->


  </div>

</section>
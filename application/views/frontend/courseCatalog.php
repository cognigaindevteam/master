<style type="text/css">
  .panel.panelhover {
    min-height: 280px;
  }
</style>

  <section id="inner-wrapper">
<div class="row" style="padding-top: 40px;">
    <div class="col-md-4">
<p style="padding-left:25px;  "><span>Home</span> > <span>Course Catalog</span></p> 
</div>
 <div class="col-md-8 col-md-push-5">
 <!--  <div class="col-md-2 active">
      <a href="javascript:void(0);"><p>Course Catalog</p></a>
  </div>
  <div class="col-md-1">
      <a href="<?php echo base_url(); ?>welcome/search"><p>Search</p></a>
  </div>
  <div class="col-md-1">
      <a href="javascript:void(0);"><p>Help</p></a>
  </div>
  <?php if($this->session->userdata('user_id')){ ?>
  <div class="col-md-2">
      <a href="<?php echo base_url(); ?>welcome/logout"><p>Log Off</p></a>
  </div> 
  <?php }?>    --> 


<ul class="nav nav-pills">
  <li class="active"><a href="<?php echo base_url(); ?>welcome/courseCatalog">Course Catalog</a></li>
  <li><a href="<?php echo base_url(); ?>welcome/search">Search</a></li>
  <li><a href="<a href="javascript:void(0);">Help</a></li>
<?php if($this->session->userdata('user_id')){ ?>
  <li><a href="<?php echo base_url(); ?>welcome/logout">Log Off</a></li>
<?php }?>
</ul>

 </div>

</div>


<?php  error_reporting(0); ?>
</section>

<section id ="feature" class="section-padding">

    <div class="container-fluid">
        <div class="row">
             <div class="">
             <div class="col-md-2" style="border-right: 1px solid #aaa; background-color: #f8f8f8;">

          <?php include('navbar.php'); ?>
             <!--   <div class="vl"></div>  -->
             </div>

                 <div class="col-md-9">
                   <div class="">
                      <div class="sidecontent">

                 <?php  if(!empty($courses)) 
                    
                  //  echo "<pre>"; print_r($courses); exit;

                 foreach ($courses as $key ) {
                 
                 ?>

                    <div class="col-md-4">

                       <section class="panel panelhover" style="box-shadow:4px 4px 15px rgba(136, 136, 136, 0.2);">
                       <form method="post" action="<?php echo base_url(); ?>welcome/makepayment">
                       <input type="hidden" name="course_id" value="<?php echo $key->id;  ?>">
                       <input type="hidden" name="course_amt" value="5000">
                    <div class="panel-body">
                       
                        <p class="form-signin-heading">Course: <?php echo $key->course_name; ?> <br>
                        Duration: 3 Months </p>


                        <div class="clearfix"></div>
                        <div class="top-stats-panel">
                            <div class="gauge-canvas">
                                <p>Start Date: <?php echo $key->training_dates; ?></p>
                                <p>Price:  $$$</p>
                              <p>Author/Trainer: <?php echo $key->trainer; ?></p>
                              <p>Description of Course: <?php echo $key->course_desc; ?></p>
                            </div>
                            <ul class="gauge-meta clearfix">
                                <li><strong class="text-xl"></strong></li>
                            </ul>
                             <?php if(array_key_exists($key->id,$enrolledCourses))
                              { ?>
                            <button  type="button" class="btn btn-primary rnroll" >Enrolled</button>
                            <?php  } else{?>
                            <button  type="submit" class="btn btn-primary rnroll" >Enroll</button>
                            <?php } ?>
                        </div>
                    </div>
                    </form>
                </section>
               </div><!--end .col -->
    <?php  } ?>

             
               <div class="col-md-4">

                      <section class="panel panelhover" style="box-shadow:4px 4px 15px rgba(136, 136, 136, 0.2);">
                    <div class="panel-body">
                       
                        <p class="form-signin-heading">Course: Stress Management <br>
                        Duration: 2 hrs </p>


                        <div class="clearfix"></div>
                        <div class="top-stats-panel">
                            <div class="gauge-canvas">
                                <p>Start Date: 11/12/17</p>
                                <p>Price:  $$$</p>
                              <p>Author/Trainer</p>
                              <p>Description of Course: Lorel ipsum Lorel ipsum Lorel ipsum</p>
                            </div>
                            <ul class="gauge-meta clearfix">
                                <li><strong class="text-xl"></strong></li>
                            </ul>
                            <button type="button" type="submit" class="btn btn-primary rnroll" ng-click="submitted = true">Enroll</button>
                        </div>
                    </div>
                </section>
               </div><!--end .col -->
             


             <!--  <div class="col-md-4">

                       <section class="panel panelhover" style="box-shadow:4px 4px 15px rgba(136, 136, 136, 0.2);">
                    <div class="panel-body">
                       
                        <p class="form-signin-heading">Course: Stress Management <br>
                        Duration: 2 hrs </p>


                        <div class="clearfix"></div>
                        <div class="top-stats-panel">
                            <div class="gauge-canvas">
                                <p>Start Date: 11/12/17</p>
                                <p>Price:  $$$</p>
                              <p>Author/Trainer</p>
                              <p>Description of Course: Lorel ipsum Lorel ipsum Lorel ipsum</p>
                            </div>
                            <ul class="gauge-meta clearfix">
                                <li><strong class="text-xl"></strong></li>
                            </ul>
                            <button type="button" type="submit" class="btn btn-primary rnroll" ng-click="submitted = true">Enroll</button>
                        </div>
                    </div>
                </section>
               </div> --><!--end .col -->

               <!--  <div class="col-md-3">

                       <section class="panel panelhover" style="box-shadow:4px 4px 15px rgba(136, 136, 136, 0.2);">
                    <div class="panel-body">
                       
                        <p class="form-signin-heading">Course: Stress Management <br>
                        Duration: 2 hrs </p>


                        <div class="clearfix"></div>
                        <div class="top-stats-panel">
                            <div class="gauge-canvas">
                                <p>Start Date: 11/12/17</p>
                                <p>Price:  $$$</p>
                              <p>Author/Trainer</p>
                              <p>Description of Course: Lorel ipsum Lorel ipsum Lorel ipsum</p>
                            </div>
                            <ul class="gauge-meta clearfix">
                                <li><strong class="text-xl"></strong></li>
                            </ul>
                            <button type="button" type="submit" class="btn btn-primary rnroll" ng-click="submitted = true">Enroll</button>
                        </div>
                    </div>
                </section>
               </div> -->
                     
                    
                   </div>
                 </div>

              


               <!-- <div class="clearfix"></div> -->

              

               <!--end .col -->

  </div>
            </section>
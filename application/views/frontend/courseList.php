  <section id="inner-wrapper">
<div class="row" style="padding-top: 40px;">
    <div class="col-md-4">
<p style="padding-left:25px;  "><span>Home</span> > <span>Enrolled Courses</span></p> 
</div>
 <div class="col-md-8 col-md-push-4">
  <div class="col-md-2">
      <a href="<?php echo base_url(); ?>welcome/courseCatalog"><p>Course Catalog</p></a>
  </div>
  <div class="col-md-1 active" id="search">
      <a href="<?php echo base_url(); ?>welcome/search" class="active"><p>Search</p></a>
  </div>
  <div class="col-md-1">
      <a href="javascript:void(0);"><p>Help</p></a>
  </div>
  <?php if($this->session->userdata('user_id')){ ?>
  <div class="col-md-2">
      <a href="<?php echo base_url(); ?>welcome/logout"><p>Log Off</p></a>
  </div> 
  <?php }?>    




 </div>

</div>
</section>
<section id ="feature" class="section-padding">
        

      <div class="container-fluid">
        
        <div class="row">
          <div class="">
           <div class="col-md-2" style="border-right: 1px solid #aaa; background-color: #f8f8f8;">
             
            <?php include('navbar.php'); ?>
             
           </div>   

          <div class="col-md-9">
              <div class="">
                 <div class="sidecontent">
                   <h4>Select Courses</h4>
                      
              <hr>
            
        

        <!-- table for search result -->

             <!--  <table class="table table-bordered"> -->
              <?php // echo "<pre>"; print_r($userCourses); exit;
              ?>

               <table id="example" class="cell-border" cellspacing="0" width="100%">
                  <thead>
                    <tr>
                      <th>ID</th>
                      <th>Course Name</th>
                      <th>Action</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php $user_id=$this->session->userdata('user_id');  foreach ($userCourses as $key ) { ?>
                      
                    
                    <tr>
                      <td><?php echo $key->course_id; ?></td>
                      <td><?php echo $key->course_name; ?></td>
                      
                    
                      <td><a href="<?php echo base_url(); ?>welcome/selectAssesments/<?php echo $key->course_id; ?>" target="_blank">Start</a></td>
                      
                    </tr>
                    <?php  }  ?>
                    
                  </tbody>
          </table>


        </div>
        </div>
        </div>
      </div>
    </section>
    <!--/ feature-->

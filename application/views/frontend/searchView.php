<div class="row" style="padding-top: 100px;">
    <div class="col-md-4">
<p style="padding-left:25px;  "><span>Home</span> > <span>Search</span></p> 
</div>
 <div class="col-md-8 col-md-push-5">
  <!-- <div class="col-md-2">
      <a href="<?php echo base_url(); ?>welcome/courseCatalog"><p>Course Catalog</p></a>
  </div>
  <div class="col-md-1 active" id="search">
      <a href="<?php echo base_url(); ?>welcome/search" class="active"><p>Search</p></a>
  </div>
  <div class="col-md-1">
      <a href="javascript:void(0);"><p>Help</p></a>
  </div>
  <?php if($this->session->userdata('user_id')){ ?>
  <div class="col-md-2">
      <a href="<?php echo base_url(); ?>welcome/logout"><p>Log Off</p></a>
  </div> 
  <?php }?>   --> 

<ul class="nav nav-pills">
  <li ><a href="<?php echo base_url(); ?>welcome/courseCatalog">Course Catalog</a></li>
  <li class="active"><a href="<?php echo base_url(); ?>welcome/search">Search</a></li>
  <li><a href="javascript:void();">Help</a></li>
<?php if($this->session->userdata('user_id')){ ?>
  <li><a href="<?php echo base_url(); ?>welcome/logout">Log Off</a></li>
<?php }?>
</ul>


 </div>

</div>
<section id ="feature" class="section-padding">
        

      <div class="container-fluid">
         
        <div class="row">
          <div class="">
            <div class="col-md-2" style="border-right: 1px solid #aaa; background-color: #f8f8f8;">
            <?php include('navbar.php'); ?>
            
             
           </div>   

          <div class="col-md-9">
              <div class="">
                 <div class="sidecontent">
                   <h4>Search Course</h4>
                      <div class="col-md-2">
                                <div class="form-group">
                                <label class="notes">Course Name</label>
                               </div>
                               <div class="form-group">
                                     <label class="notes">Trainer Name</label>
                                 </div>
                                 <div class="form-group">
                                     <label class="notes">Training Dates</label>
                                 </div>
                           </div>
             <form    method="post" action="<?php echo base_url(); ?>welcome/searchResult ">
             <div class="col-md-4">
                       
                       <div class="form-group">
                            <input type="text" class="form-control" placeholder="Course Name"  name="cName" >
                               

                      </div>
                      <div class="form-group">
                            <input type="text" class="form-control" placeholder="Trainer Name"  name="tname" >
                              

                      </div>

                      <div class="form-group">
                          <input type="text" class="form-control" placeholder="Training Dates"  name="tdate"  >
                             

                      </div>
                     <button type="submit" class="btn btn-primary">Search</button>

             </div>
 </form>
             
        </div>
        </div>
        <hr>

        <!-- table for search result -->

        <?php    error_reporting(0);
      //  echo "<pre>"; print_r($searchedResult); // exit; 
          if(!empty($searchedResult)) {   ?>

            <table id="example" class="cell-border" cellspacing="0" width="100%">
                  <thead>
                    <tr>
                      <th>Sr.No.</th>
                      <th>Course Name</th>
                      <th>Type</th>
                      <th>Trainer</th>
                      <th>Durartion</th>
                      <!-- <th>Status</th>
                      <th>Action</th> -->
                    </tr>
                  </thead>
                  <tbody>
                  <?php $i=1; foreach ($searchedResult as $key ) {
                    $i++;
                   ?>
                    <tr>
                      <td><?php echo $i; ?></td>
                      <!-- <td><a href="<?php echo base_url(); ?>welcome/courseDetail/<?php echo $key->id; ?>"><?php echo $key->course_name; ?></a></td> -->
                      <td><a href="<?php echo base_url(); ?>welcome/courseCatalog"><?php echo $key->course_name; ?></a></td>
                      <td><?php echo $key->type_of_course; ?></td>
                      <td><?php echo $key->trainer; ?></td>
                      <td>2 hrs</td>
                     <!--  <td>In Progress</td>
                      <td>Status</td> -->
                      
                    </tr>
                    <?php  }?>
                  <!--   <tr>
                      <td>002</td>
                      <td>PGDHHM</td>
                      <td>Web Based</td>
                      <td>Mr. Jhon</td>
                      <td>2 hrs</td>
                      <td>In Progress</td>
                      <td>Status</td>
                    </tr>
                    <tr>
                      <td>003</td>
                      <td>PGDHHM</td>
                      <td>Web Based</td>
                      <td>Mr. Jhon</td>
                      <td>2 hrs</td>
                      <td>In Progress</td>
                      <td>Resume</td>
                    </tr> -->
                  </tbody>
          </table> 
<?php } ?>

        </div>
        </div>
        </div>
      </div>
    </section>
    <!--/ feature-->

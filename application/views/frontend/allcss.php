<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">

    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>cognigain</title>
    <meta name="description" content="Free Bootstrap Theme by BootstrapMade.com">
    <meta name="keywords" content="free website templates, free bootstrap themes, free template, free bootstrap, free website template">
    
    <!-- <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Open+Sans|Candal|Alegreya+Sans"> -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>frontAssets/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>frontAssets/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>frontAssets/css/imagehover.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>frontAssets/css/jquery.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>frontAssets/css/style.css">
   
    <!-- load angularjs library -->
       <script type="text/javascript" src="<?php echo base_url(); ?>frontAssets/js/angular.min.js"></script>
  

  </head>
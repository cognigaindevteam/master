<style type="text/css">

.gap{

	padding-top: 11px;
}
.nav.nav-pills.nav-stacked.courseNav
{
	width:150px;
}
#coursedesc {
	/*margin-top: 50px;*/
	/*margin-bottom: 50px;*/
	background-color: #f4f4f4;
}
.coursecontent {
	padding-top: 15px;
	padding-bottom: 15px;
	background-color: #fff;
	min-height: 600px;
	border: 1px solid #ddd;
}
.coursename {
	background-color: #fff;
}
.coursetopics {
	min-height: 600px;
	padding-top: 22px;
	padding-bottom: 22px;
	padding-left: 0;
	padding-right: 0;
	border-top: 1px solid #ddd;
	border-bottom: 1px solid #ddd;
}
.coursetopics .nav-pills > li > a {
    border-radius: 0;
}
.coursetopics .nav-pills > li.active > a {
	background-color: #1e73be;
	color: #fff;
}
.coursetopics .nav-pills > li > a {
	color: #333;
	font-weight: 600;
}
</style>
<?php  //echo "<pre>"; print_r($courseTopics); exit; ?>


	  <section id="inner-wrapper">
<div class="row" style="padding-top: 40px;">
    <div class="col-md-4">
<!-- <p style="padding-left:25px;  "><span>Home</span> > <span>My Dashboard</span></p>  -->
</div>
 <div class="col-md-8 col-md-push-4">
  <div class="col-md-2">
      <a href="<?php echo base_url(); ?>welcome/courseCatalog"><p>Course Catalog</p></a>
  </div>
  <div class="col-md-1 active" id="search">
      <a href="<?php echo base_url(); ?>welcome/search"><p>Search</p></a>
  </div>
  <div class="col-md-1">
      <a href="javascript:void(0);"><p>Help</p></a>
  </div>
  <?php if($this->session->userdata('user_id')){ ?>
  <div class="col-md-2">
      <a href="<?php echo base_url(); ?>welcome/logout"><p>Log Off</p></a>
  </div> 
  <?php }?>

 </div>

</div>
</section>


<section id="coursedesc">

	<div class="container-fluid">
		
		<!-- <div class="gap"></div> -->

		<div class="row">
			<div class="col-md-12 coursename">

				<h4><strong>Course Name: </strong><?php 
				error_reporting(0);  
				echo $courseName->course_name; ?></h4> 

			</div>

			<div class="col-md-2 coursetopics">
				<ul class="nav nav-pills nav-stacked" id="coursetopics">

					<?php 

					foreach ($courseTopics as $k=>$key) { ?>
                   
                     <li id="topic_name_<?php echo $key->id; ?>" class="<?php echo $k=='0' ? 'active' : '' ?><?php echo $k==0 ? ' coursetopics0' : '' ?>"><a data-toggle="pill" class="content"  data-id="<?php  echo 'desc'.$key->id;?>" href="<?php echo '#topic'.$key->id; ?>"><?php echo $key->topic_name; ?>  <!-- <i class="fa fa-angle-right pull-right"></i> --></a></li>
                     		    
				   <?php	} ?>
                      

					
				<li><a id="assesnment" href="<?php echo base_url(); ?>welcome/startAssesments/<?php echo $this->session->userdata('cId'); ?>">Take Assesments</a></li>
				
				</ul>
			</div>

			<div class="col-md-10 coursecontent" id="loadQuiz">
				<div class="tab-content content">
					<?php 
					foreach ($courseTopics as $k=>$key1){ 
						$id = $key1->id;
					 ?>

						<div id="<?php echo'topic'.$key1->id; ?>" class="tab-pane fixht fade in <?php echo $k=='0' ? 'active' : '' ?>">
						<div class="content" data-id="<?php  echo 'desc'.$key->id;?>">
							<h4><?php echo $key1->topic_name; ?></h4>
							<?php echo $key1->topic_desc; ?>
						</div>
	                    <div class="row prvnxtbtn">
	                    	<div class="col-md-12">
	                    		<div class="pull-right" style="margin-right: 39px;">
	                    		<!-- <a data-toggle="pill" href="<?php if($key1->id==1){ echo  '#topic'.$key1->id;}else{echo  '#topic'.$key1->id--;} ?>"><button type="button" class="btn btn-primary">Previous</button></a>
	                    		<a data-toggle="pill" href="<?php echo '#topic'.$key1->id++; ?>">	<button type="button" class="btn btn-success">Next</button></a> -->
	                    		<?php if($k!=0){ ?>
	                    		<a data-toggle="pill" href="javascript:void(0)" class="btn btn-primary back" data="<?php echo $id; ?>">Previous</a> 
	                    		<?php } ?>
	                    		<a data-toggle="pill" href="javascript:void(0)" class="btn btn-success next" data="<?php echo $id; ?>">Next</a>
	                    		</div>
	                    	</div>
	                    </div>

					</div>
					<?php  } ?>
					


				</div>

			</div>

		</div>

	</div>

</section>